Import("env")

print(env.Dump())

board_config = env.BoardConfig()

board_config.update("build.hwids", [
    #VID      PID
  ["0xAAAA", "0x456"]
])
board_config.update("vendor", "MyCustomVendor")
board_config.update("build.usb_product", "MyCustomProduct")
