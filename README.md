# Custom USB device ID for Arduino Pro Micro

This project is a small template for projects which need the Arduino Pro Micro to have a custom USB product ID or vendor ID (PID and VID). For example if you have multiple Pro Micros connected and need to identify them individually.

## Usage
To configure the PID and VID just edit the `conifg.py` script. The first part of the array the VID, the second part being the PID. You can also configure the usb product and vendor name in the same script. However lsusb will not show the new vendor name as it uses the VID to get this. Using dmesg you will however see both the new vendor and product name.

You can then upload the script with the usual `pio run --target upload`. Executing a lsusb should show the new PID and VID.

To use the custom PID and VID you can use the provided udev rule to assign this Pro Micro to a specific tty device. Just edit the `idVendor` and `idProduct` field in the rules file to match the ones given to the Pro Micro.
